function salaryCalculating() {
    var dailyWages = parseInt(document.getElementById("dailyWages").value);
    var workingDays = parseInt(document.getElementById("workingDays").value);

    var salary = new Intl.NumberFormat('vn-VN').format(dailyWages * workingDays); 

    // var salaryValue = `<p>Tổng tiền lương của bạn là: ${salary}</p>`

    document.getElementById("txt-salary").innerText = salary;
}

function averageNumber(){
    var firstNumber = parseInt(document.getElementById("firstNumber").value);
    var secondNumber = parseInt(document.getElementById("secondNumber").value);
    var thirdNumber = parseInt(document.getElementById("thirdNumber").value);
    var fourthNumber = parseInt(document.getElementById("fourthNumber").value);
    var fifthNumber = parseInt(document.getElementById("fifthNumber").value);

    var averageNumber = new Intl.NumberFormat('vn-VN').format(( firstNumber + secondNumber + thirdNumber + fourthNumber +fifthNumber)/5);

    // var averageValue = `<p>Giá trị trung bình của bạn là: ${averageNumber}</p>`

    document.getElementById("txt-average").innerText = averageNumber;
}

function moneyConvert() {
    var usdRate = 23500;
    var usdAmount = parseInt(document.getElementById("usdAmount").value);
    var vietnamDong = new Intl.NumberFormat('vn-VN').format(usdRate * usdAmount); 

    // var vietnamdongValue = `<p>Số tiền của bạn là ${vietnamDong} </p>`

    document.getElementById("txt-vietnamDong").innerText = vietnamDong;
}

function rectangleCalculating() {
    var rectangleLength = parseInt(document.getElementById("rectangleLength").value);
    var rectangBreadth = parseInt(document.getElementById("rectangleBreadth").value);
    var rectangleArea = new Intl.NumberFormat('vn-VN').format(rectangBreadth * rectangleLength );
    var rectanglePerimeter = new Intl.NumberFormat('vn-VN').format((rectangleLength + rectangBreadth)*2);

    // var rectangleResult = `<p>Diện tích: ${rectangleArea}, Chu vi: ${rectanglePerimeter}</p> `

    document.getElementById("txt-rectangleResult").innerText = `Diện tích: ${rectangleArea}, Chu vi: ${rectanglePerimeter}`
}

function sumOf2(){
    var baseNumber = parseInt(document.getElementById("baseNumber").value);
    var unitNumber = baseNumber % 10;
    var tensNumber = Math.floor(baseNumber / 10);

    var sumOf2Result = tensNumber + unitNumber;

    // var sumOf2Value = `<p>Tổng 2 ký số là: ${sumOf2Result}</p>`

    document.getElementById("txt-sumOf2Result").innerText = sumOf2Result; 
}


